package com.cibertec.sesion02app

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class DestinationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_destination)

        //1. Reciba el Bundle enviado
        val bundle  = intent.extras

        val names = bundle?.getString("KEY_NAMES") ?: "Desconocido"
        val age = bundle?.getString("KEY_AGE") ?: "Desconocido"
        val gender = bundle?.getString("KEY_GENDER") ?: "Desconocido"

        println("Nombres: $names - Edad: $age - Genero: $gender")

        //Scope Functions LET, APPLY, RUN, WITH

        /*bundle?.let { bundleLibreDeNulos ->
            val names = bundleLibreDeNulos.getString("KEY_NAMES") ?: "Desconocido"
            val age = bundleLibreDeNulos.getString("KEY_AGE") ?: "Desconocido"
            val gender = bundleLibreDeNulos.getString("KEY_GENDER") ?: "Desconocido"

            println("Nombres: $names - Edad: $age - Genero: $gender")
        } ?: kotlin.run {

        }*/

        /*if(bundle!=null) {
            val names = bundle.getString("KEY_NAMES") ?: "Desconocido"
            val age = bundle.getString("KEY_AGE") ?: "Desconocido"
            val gender = bundle.getString("KEY_GENDER") ?: "Desconocido"

            println("Nombres: $names - Edad: $age - Genero: $gender")
        }*/



    }
}