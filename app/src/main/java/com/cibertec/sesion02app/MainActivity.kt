package com.cibertec.sesion02app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.CheckBox
import android.widget.RadioButton
import android.widget.Toast
import com.google.android.material.textfield.TextInputEditText

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Inyeccion de Vistas
        //findViewById
        //Kotlin extensions (Deprecated)
        //ViewBindings

        //1. Inyectar las vistas
        val btnSend : Button = findViewById(R.id.btnSend)
        val edtNames : TextInputEditText = findViewById(R.id.edtA)
        val edtAge : TextInputEditText = findViewById(R.id.edtB)
        val rbMale : RadioButton = findViewById(R.id.rbMale)
        val rbFemale : RadioButton = findViewById(R.id.rbFemale)
        val chkTerms : CheckBox = findViewById(R.id.chkTerms)

        //2. Evento Onclick del boton
        btnSend.setOnClickListener {

            //3. Obtener los datos
            val names = edtNames.text.toString()
            val age = edtAge.text.toString()
            val gender = if(rbMale.isChecked) "Masculino" else "Femenino"

            //4. Validar
            if(names.isEmpty()){
                //Toast.makeText(this,getString(R.string.activity_main_validator_names),Toast.LENGTH_LONG).show()
                toast(getString(R.string.activity_main_validator_names))
                return@setOnClickListener
            }

            if(age.isEmpty()){
                //Toast.makeText(this,getString(R.string.activity_main_validator_age),Toast.LENGTH_LONG).show()
                toast(getString(R.string.activity_main_validator_age))
                return@setOnClickListener
            }

            if(!chkTerms.isChecked){
                //Toast.makeText(this,getString(R.string.activity_main_validator_terms),Toast.LENGTH_LONG).show()
                toast(getString(R.string.activity_main_validator_terms))
                return@setOnClickListener
            }

            //APPLY

            //5. Guardar los datos
            /*val bundle = Bundle()
            bundle.putString("KEY_NAMES",names)
            bundle.putString("KEY_AGE",age)
            bundle.putString("KEY_GENDER",gender)*/

            val bundle = Bundle().apply {
                putString("KEY_NAMES",names)
                putString("KEY_AGE",age)
                putString("KEY_GENDER",gender)
            }

            //6. Navegar a la pantalla destino
            val intent = Intent(this,DestinationActivity::class.java)
            intent.putExtras(bundle)
            startActivity(intent)


            //Validacion que Masculino y Femenino no esten seleccionados
            //(!rbMale.isChecked && !rbFemale.isChecked){
            //}
        }
    }

    fun add(number1:Int,number2:Int) : Int {
        return number1 + number2
    }
    fun add2(number1:Int,number2:Int) = number1 + number2


    fun hello(name:String) : Unit {
        println("Hello $name")
    }
    fun hello2(name:String) = println("Hello $name")

    fun toast(message:String) = Toast.makeText(this,message,Toast.LENGTH_LONG).show()

}